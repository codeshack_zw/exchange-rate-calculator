function convertCurrency() {
    if ($( "#fromCurrency option:selected" ).val() !== "" &&
        $( "#toCurrency option:selected" ).val() !== "" &&
        $("#fromCurrencyAmount").val()!== "") {
        
        if (isNaN($("#fromCurrencyAmount").val())) {
            //invalid amount provided
            alert("Please enter a numeric amount.");
            $("#fromCurrencyAmount").focus();
            return;
        }
        
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var response = $.parseJSON(xhr.responseText);
                if(response["error"] == true){
                    //an error occured.
                    alert(response["error_msg"]);
                }else{
                    //successful request.
                    $("#convertedAmount").val(response["converted_amount"]);
                    $("#exchangeRateUsed").html("Exchange Rate: " + response["rate_used"]);
                }
            }
        };
        
        xhr.onerror = function(){
            alert("An error occured while trying to convert the currencies.");
            return;
        }
       
        var parm = "fromCur="+  $( "#fromCurrency option:selected" ).val() + 
                   "&toCur=" + $( "#toCurrency option:selected" ).val() +
                   "&amount=" + $("#fromCurrencyAmount").val();
        xhr.open("POST", "/exchange/convertCurrency.php", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(parm);
    }else{
        //all required fields not provided
        alert("Please make sure you have the currencies to be used for conversion selected and also enter an amount to convert.");
    }
}