<?php
/*
 * infosight exchange rate converter
 * Russell Mazonde
 *
 */

$apiURL = "https://finance.google.com/finance/converter";

//get post parms and validate them
$_FROMCUR = $_POST['fromCur']; // get post values
if(!isset($_FROMCUR)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid from currency parameter provided.";
    echo json_encode($response);
    return;
}

$_TOCUR = $_POST['toCur']; // get post values
if(!isset($_TOCUR)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid to currency parameter provided.";
    echo json_encode($response);
    return;
}

$_AMOUNT = $_POST['amount']; // get post values
if(!isset($_AMOUNT)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid amount parameter provided.";
    echo json_encode($response);
    return;
}

//get currency conversion rate
$converted_currency = currencyConverter($_FROMCUR, $_TOCUR);

//convert using rate from api
if($converted_currency){
    $_AMOUNT *= $converted_currency;
    
    $response["error"] = false;
    $response["converted_amount"] = "$_AMOUNT";
    $response["rate_used"] = "$converted_currency";
    echo json_encode($response);
    return;
}else{
    $response["error"] = false;
    $response["error_msg"] = "Error occured trying to retrieve conversion rate";
    echo json_encode($response);
    return;
}

//perform currency calculation. No rounding or truncation needed
function currencyConverter($from_Currency, $to_Currency) {
    //html encode parms
    global $apiURL;
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);
    
    //perform get request on api URL 
    $get = file_get_contents("$apiURL?a=1&from=$from_Currency&to=$to_Currency");
    $get = explode("<span class=bld>",$get);
    $get = explode("</span>",$get[1]);
    
    //get rate and return
    $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
    return $converted_currency;
}
?>